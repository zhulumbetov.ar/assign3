window.addEventListener('DOMContentLoaded', (event) => {
    const daySelect = document.getElementById('daySelect');
    const addDayButton = document.getElementById('addDayButton');
    const daysContainer = document.getElementById('daysContainer');

    addDayButton.addEventListener('click', () => {
        const selectedDay = daySelect.value;
        createDay(selectedDay);
    });

    function createDay(day) {
        const dayElement = document.createElement('div');
        dayElement.classList.add('day');
        dayElement.innerHTML = `
            <h2>${day}</h2>
            <button class="deleteDayButton">Delete Day</button>
            <div class="casesContainer"></div>
            <div class="addCaseContainer">
                <button class="addCaseButton">Add Case</button>
                <div class="caseInputWrapper">
                    <input type="text" placeholder="Enter case">
                </div>
            </div>
        `;

        daysContainer.appendChild(dayElement);

        const deleteDayButton = dayElement.querySelector('.deleteDayButton');
        deleteDayButton.addEventListener('click', () => {
            dayElement.remove();
        });

        const addCaseContainer = dayElement.querySelector('.addCaseContainer');
        const addCaseButton = addCaseContainer.querySelector('.addCaseButton');
        const caseInputWrapper = addCaseContainer.querySelector('.caseInputWrapper');
        const caseInput = caseInputWrapper.querySelector('input[type="text"]');

        addCaseButton.addEventListener('click', () => {
            if (caseInputWrapper.style.display === 'none') {
                caseInputWrapper.style.display = 'block';
                caseInput.focus();
                addCaseButton.innerText = 'Save Case';
            } else {
                const caseValue = caseInput.value;
                if (caseValue.trim() !== '') {
                    createCase(dayElement, caseValue);
                    caseInput.value = '';
                }
                caseInputWrapper.style.display = 'none';
                addCaseButton.innerText = 'Add Case';
            }
        });

        const casesContainer = dayElement.querySelector('.casesContainer');

        function createCase(dayElement, caseValue) {
            const caseElement = document.createElement('div');
            caseElement.classList.add('case');
            caseElement.innerHTML = `
                <span>${caseValue}</span>
                <button class="deleteCaseButton">Delete Case</button>
            `;

            casesContainer.appendChild(caseElement);

            const deleteCaseButton = caseElement.querySelector('.deleteCaseButton');
            deleteCaseButton.addEventListener('click', () => {
                caseElement.remove();
            });
        }
    }
});
